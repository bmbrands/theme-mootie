<?php

/********************************************************
**
** Theme name: Mootie
** Creation Date: 16 march 2012
** Author: Bas Brands
** Author URI: http://www.sonsbeekmedia.nl
**
*********************************************************/ 

class theme_mootie_core_renderer extends core_renderer {
	
	protected function render_custom_menu(custom_menu $menu) {
		// If the menu has no children return an empty string
		if (!$menu->has_children()) {
			return '';
		}
		// Initialise this custom menu
		$content = html_writer::start_tag('div', array('id'=>'navigation'));
		$content .= html_writer::start_tag('ul', array('id'=>'main-navigation'));
		// Render each child
		foreach ($menu->get_children() as $item) {
			$content .= $this->render_custom_menu_item($item);
		}
		// Close the open tags
		$content .= html_writer::end_tag('ul');
		$content .= html_writer::end_tag('div');
		// Return the custom menu
		return $content;
	}

	protected function render_custom_menu_item(custom_menu_item $menunode) {
		// Required to ensure we get unique trackable id's
		static $submenucount = 0;
		$content = html_writer::start_tag('li');
		if ($menunode->has_children()) {
			// If the child has menus render it as a sub menu
			$submenucount++;
			if ($menunode->get_url() !== null) {
				$url = $menunode->get_url();
			} else {
				$url = '#cm_submenu_'.$submenucount;
			}
			
			$content .= html_writer::link($url, $menunode->get_text(), array('title'=>$menunode->get_title()));
			
			$content .= html_writer::start_tag('ul');
			foreach ($menunode->get_children() as $menunode) {
				$content .= $this->render_custom_menu_item($menunode);
			}
			$content .= html_writer::end_tag('ul');
		} else {
			// The node doesn't have children so produce a final menuitem

			if ($menunode->get_url() !== null) {
				$url = $menunode->get_url();
			} else {
				$url = '#';
			}
			$content .= html_writer::link($url, $menunode->get_text(), array('title'=>$menunode->get_title()));
		}
		$content .= html_writer::end_tag('li');
		// Return the sub menu
		return $content;
	}
	
	public function heading($text, $level = 2, $classes = 'main', $id = null) {
		global $COURSE;
		 
		$topicoutline = get_string('topicoutline');

		if ($text == $topicoutline) {
			$text = $COURSE->fullname;
		}
		
		if ($text == get_string('weeklyoutline')) {
			$text = $COURSE->fullname;
		}

		$content = parent::heading($text, $level, $classes, $id);

		return $content;
	}
}
