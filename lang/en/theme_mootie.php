<?php


/********************************************************
**
** Theme name: Mootie
** Creation Date: 16 march 2012
** Author: Bas Brands
** Author URI: http://www.sonsbeekmedia.nl
**
*********************************************************/ 

$string['pluginname'] = 'Mootie';
$string['region-side-post'] = 'Right';
$string['region-side-pre'] = 'Left';
$string['choosereadme'] = '<div class="clearfix">
<div class="theme_screenshot">
<h2>Mootie</h2>
<img src="mootie/pix/screenshot.jpg" />
<h3>Based on sources from</h3>
<p><a href="http://moodlemoot.ie">The Moodle Moot website</a></p>
<h3>For more themes and info contact</h3>
<p><a href="http://www.linkedin.com/in/gavinhenrick">Gavin Henrick</a></p>
<p><a href="nl.linkedin.com/pub/bas-brands/16/404/319">Bas Brands</a></p>
</div>
<div class="theme_description">
<h2>About</h2>
<p>The Mootie theme is based on the Leatherbound theme for Moodle 2.0
that was created by Patrick Malley.
<h2>Tweaks</h2>
<p>This theme is built upon both Base and Canvas, two parent themes
included in the Moodle core. If you want to modify this theme, we
recommend that you first duplicate it then rename it before making your
changes. This will prevent your customized theme from being overwritten
by future Moodle upgrades, and you\'ll still have the original files if
you make a mess. More information on modifying themes can be found in
the <a href="http://docs.moodle.org/en/Theme">MoodleDocs</a>.</p>
<h2>Credits</h2>
<p>This theme was coded and is maintained by Bas Brands of
Sonsbeekmedia.</p>
</div>
</div>';
$string['footerlinks'] = 'Footer Links';
$string['footerlinksdesc'] = 'Edit links displayed in the pages footer';
$string['gakey'] = 'Google Analitics key';
$string['gakeydesc'] = 'Get a google analytics code for your site from http://google.com/analytics';