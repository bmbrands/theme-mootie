<?php

/**
 * Settings for the mootie theme
 */

defined('MOODLE_INTERNAL') || die;

if ($ADMIN->fulltree) {
	
	// gakey
	$name = 'theme_mootie/gakey';
	$title = get_string('gakey','theme_mootie');
	$description = get_string('gakeydesc', 'theme_mootie');
	$setting = new admin_setting_configtext($name, $title, $description, '');
	$settings->add($setting);	
	
	// footerlinks
	$name = 'theme_mootie/footerlinks';
	$title = get_string('footerlinks','theme_mootie');
	$description = get_string('footerlinksdesc', 'theme_mootie');
	$setting = new admin_setting_confightmleditor($name, $title, $description, '');
	$settings->add($setting);
}