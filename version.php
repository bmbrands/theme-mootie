<?php

/********************************************************
**
** Theme name: mootie, based on Leatherbound by Patrick Malley
** Creation Date: 16 march 2012
** Author: Bas Brands
** Author URI: http://www.sonsbeekmedia.nl
**
*********************************************************/ 

defined('MOODLE_INTERNAL') || die;

$plugin->version  = 2011061301;  // The current module version (Date: YYYYMMDDXX)
$plugin->component = 'theme_mootie';
$plugin->requires = 2010080300;  // Requires this Moodle version
$plugin->maturity  = MATURITY_STABLE;